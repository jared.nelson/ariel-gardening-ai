using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planter : MonoBehaviour
{
    //Global variable used in script to hold conditions
    //Numbers represent condition of conditions in order of: lime deposit, water, light, temperature, and decay after fruiting
    int[] planterTWOHash = {0, 0, 0, 0, 0};
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void careCheck()
    {
        int userInput = 0;
        
        //Waiting for tool implementation
        int lightlevel = 0;

        switch(userInput) 
            {
            case 1:
                int y = planterTWOHash[0];
                y = y + 5;
                planterTWOHash[0] = y;
                break;
            case 2:
                int y = planterTWOHash[1];
                y = y + 5;
                planterTWOHash[1] = y;
                break;
            case 3:
                if(lightlevel > 3 || lightlevel < 7)
                    {
                        planterTWOHash[3] = lightlevel;
                    }
                break;
            }

    }

    private void FixedUpdate()
    {
        int w = 0;
        int t = 0;
        int potTWOHash = 220000022;

        //Lowers conditions over time
        while (w < 5)
        {
            int y = planterTWOHash[w];
            y = y - 1;
            planterTWOHash[w] = y;
            w++; 
        }

        //Changes hash to be used by the savefile script
        while (t < 5)
        {
            switch(t)
            {
                case 0:
                    int p = planterTWOHash[t];
                    p = p * 1000000;
                    potTWOHash = potTWOHash + p;
                break;
                case 1:
                    int p = planterTWOHash[t];
                    p = p * 100000;
                    potTWOHash = potTWOHash + p;
                break;
                case 2:
                    int p = planterTWOHash[t];
                    p = p * 10000;
                    potTWOHash = potTWOHash + p;
                break;
                case 3:
                    int p = planterTWOHash[t];
                    p = p * 1000;
                    potTWOHash = potTWOHash + p;
                break;
                case 4:
                    int p = planterTWOHash[t];
                    p = p * 100;
                    potTWOHash = potTWOHash + p;
                break;
            }
            
            t++;
        }
    }
}
