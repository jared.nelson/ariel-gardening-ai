using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planter : MonoBehaviour
{
    //Global variable used in script to hold conditions
    //Numbers represent condition of conditions in order of: lime deposit, water, light, temperature, and decay after fruiting
    int[] planterOneHash = {0, 0, 0, 0, 0};
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void careCheck()
    {
        int userInput = 0;
        
        //Waiting for tool implementation
        int lightlevel = 0;

        switch(userInput) 
            {
            case 1:
                int y = planterOneHash[0];
                y = y + 5;
                planterOneHash[0] = y;
                break;
            case 2:
                int y = planterOneHash[1];
                y = y + 5;
                planterOneHash[1] = y;
                break;
            case 3:
                if(lightlevel > 3 || lightlevel < 7)
                    {
                        planterOneHash[3] = lightlevel;
                    }
                break;
            }

    }

    private void FixedUpdate()
    {
        int w = 0;
        int t = 0;
        int potONEHash = 220000022;

        //Lowers conditions over time
        while (w < 5)
        {
            int y = planterOneHash[w];
            y = y - 1;
            planterOneHash[w] = y;
            w++; 
        }

        //Changes hash to be used by the savefile script
        while (t < 5)
        {
            switch(t)
            {
                case 0:
                    int p = planterOneHash[t];
                    p = p * 1000000;
                    potONEHash = potONEHash + p;
                break;
                case 1:
                    int p = planterOneHash[t];
                    p = p * 100000;
                    potONEHash = potONEHash + p;
                break;
                case 2:
                    int p = planterOneHash[t];
                    p = p * 10000;
                    potONEHash = potONEHash + p;
                break;
                case 3:
                    int p = planterOneHash[t];
                    p = p * 1000;
                    potONEHash = potONEHash + p;
                break;
                case 4:
                    int p = planterOneHash[t];
                    p = p * 100;
                    potONEHash = potONEHash + p;
                break;
            }
            
            t++;
        }
    }
}
