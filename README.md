# Ariel Gardening AI

An A.I. designed to take care of an automated greenhouse garden.

## Description

This project is designed to show the systems of an artificial intelligence that regulates the many needs of each plant located in an automated greenhouse. It is designed to recognize specific patterns and learn from users as how to proceed when patterns arise in order to self automate.

## Main Tasks

- Create Tilemap and sprites to be used for simulation
- Add scripts to objects
- Program timed object effects
- Add custom, random events
- Create Menu
- Create save file using automated Unity file and/or XML file

## Recent Changes

- Reformatted code to be in C# instead of C++
- Updated Documentation
- Added XML creation script for experimentation

## Project Status

Currently under construction

### Please note that this is a school project. Any and all files are for educational use only.