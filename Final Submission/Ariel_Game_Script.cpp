#include <stdio.h>
#include <iostream>
#include <fstream>
using namespace std;

//Variables used in the play portion of the code.
int plant1 = 10000;
int plant2 = 10000;
int plant3 = 10000;
int plant4 = 10000;
int plant5 = 10000;
int plant6 = 10000;

int session = 0;
int death1 = 0;
int death2 = 0;
int death3 = 0;
int death4 = 0;
int death5 = 0;
int death6 = 0;

//User menu that decides the values that test the A.I.
void plantMenus()
{
    int x = 0;
    int y = 0;
    int z = 0;

    for (int i = 0; i < 6; i++)
    {
        cout << "Plant Levels\n";
        cout << "Water Level:\n";
        cin >> x;
        cout << "Lime Level:\n";
        cin >> y;
        cout << "UV Level:\n";
        cin >> z;

        x = x * 1000;
        y = y * 100;
        z = z * 10;

        switch(i) 
        {
            case 0:
                plant1 = plant1 + x + y + z;
                break;
            case 1:
                plant2 = plant1 + x + y + z;
                break;
            case 2:
                plant3 = plant1 + x + y + z;
                break;
            case 3:
                plant4 = plant1 + x + y + z;
                break;
            case 4:
                plant5 = plant1 + x + y + z;
                break;
            case 5:
                plant6 = plant1 + x + y + z;
                break;
            default:
                //error code here
        }
    }

};
//The optional menu to delete the A.I. files
//Not Fully Working Yet
void aiOptions()
{
    int ans;
    cout << "Do you wish to erase all save data for the A.I.?\n";
    cout << "1. Yes\n";
    cout << "2. No\n";
    cin >> ans;

    if(ans == 1)
    {
        deleteAI();
    }
    else if(ans == 2)
    {
        return 0;
    }
    else
    {
            //error code here
    }
};
//The menu to check if the user wanted this option to change plant values
void plantOptions()
{
    cout << "The following menus will give you choices to input a number between 0-9 for water, lime, and\n";
    cout << "UV rays for six plants. Choose numbers to make the A.I. work. Do you wish to continue?"
    cout << "1. Yes\n";
    cout << "2. No\n";
    cin >> ans;

    if(ans == 1)
    {
        plantMenus();
    }
    else if(ans == 2)
    {
        return 0;
    }
    else
    {

    }
};
//This would have checked the score save file and allowed the user to add theirs
void scoreCheck()
{
    //unfinished
};
//This would have been where the A.I. checks each plant for it's vales and rechecks every loop
void plotCheck();
{
    for (int i = 0; i < 6; i++)
    {
        
        
        switch(i) 
        {
            case 0:
                plant1 = plant1 + x + y + z;
                break;
            case 1:
                plant2 = plant1 + x + y + z;
                break;
            case 2:
                plant3 = plant1 + x + y + z;
                break;
            case 3:
                plant4 = plant1 + x + y + z;
                break;
            case 4:
                plant5 = plant1 + x + y + z;
                break;
            case 5:
                plant6 = plant1 + x + y + z;
                break;
            default:
                //error code here
        }
    }
};
//This is the representation of the game progressing by displaying the failed attempts of the A.I. and calculating the score
void gamePresent()
{
     cout << "***";
        cout << "|||Plant 1||| Deaths: " << death1 << endl;
        cout << "|||Plant 2||| Deaths: " << death2 << endl;
        cout << "|||Plant 3||| Deaths: " << death3 << endl;
        cout << "|||Plant 4||| Deaths: " << death4 << endl;
        cout << "|||Plant 5||| Deaths: " << death5 << endl;
        cout << "|||Plant 6||| Deaths: " << death6 << endl;
        cout << "***";
};
//This function displays the current high scores
void currentScores()
{
    string scoreText;

    ifstream MyReadFile("scores.txt");

    while (getline (MyReadFile, myText)) 
    {
        // Output the text from the file
         cout << myText;
    }

    MyReadFile.close();
};
//This is the main game function loop to display the A.I. working
void gameLoop()
{
    while(session == 0)
    {
        gamePresent();
        plotCheck():
    }

    scoreCheck();
};
//This would have deleted the save file for the A.I.
void deleteAI()
{
    return 0;
};

//Main Menu function
int main()
{
    int menuChoice;
    
    //Algorithm Menu
    while(menuChoice != 5)
    {
        cout << "Garden Game Menu:\n";
        cout << "\n";
        cout << "1. Play\n";
        cout << "2. A.I. Options\n";
        cout << "3. Plant Options\n";
        cout << "4. Scores\n";
        cout << "5. Exit\n";
        cout << "\n";
        cin >> menuChoice;
        cout << "\n";
        
        if(menuChoice == 1)
        {
            gameLoop();
        }
        
        if(menuChoice == 2)
        {
            aiOptions();
        }
        
        if(menuChoice == 3)
        {
            plantOptions();
        }
        
        if(menuChoice == 4)
        {
            currentScores();
        }
        
        if(menuChoice == 5)
        {
            exit;
        }
    }
}