using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plotScript : MonoBehaviour
{

    bool isPlanted = false;
    public SpriteRenderer plant;


    int plantForm = 0;
    float timer;

    public PlantObject selectedPlant;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlanted)
        {
            timer -= Time.deltaTime;

            if (timer < 0 && plantForm<selectedPlant.plantForms.Length-1)
            {
                timer = selectedPlant.timeamount;
                plantForm++;
                UpdatePlant();
            }
        }
    }

    private void OnMouseDown()
    {
        if (isPlanted)
        {
           if (plantForm == selectedPlant.plantForms.Length - 1)
            { 
            Collect();
            }
        }
        else
        {
            Plant();
        }
    }

    void Collect()
    {
        isPlanted = false;
        plant.gameObject.SetActive(false);
       // fm.Transaction(selectedPlant.sellPrice);
       // isDry = true;
       // plot.sprite = drySprite;
      //  speed = 1f;
    }

    void Plant()
    {
        isPlanted = true;
        plant.gameObject.SetActive(true);
        plantForm = 0;
        UpdatePlant();
        timer = selectedPlant.timeamount;
       // fm.Transaction(selectedPlant.sellPrice);
       // isDry = true;
       // plot.sprite = drySprite;
      //  speed = 1f;
    }

    void UpdatePlant()
    {
        
        plant.sprite = selectedPlant.plantForms[plantForm];

    }
}
