using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        var namePlants = new Dictionary<int, string>();

        namePlants.Add(1,"Empty");
	    namePlants.Add(2,"aloeSEED");
	    namePlants.Add(3,"oranSEED");
	    namePlants.Add(4,"potatoSEED");
	    namePlants.Add(5,"echinaceaSEED");
	    namePlants.Add(6,"lavenderSEED");
	    namePlants.Add(7,"chamomileSEED");
	    namePlants.Add(8,"krishnaSEED");
	    namePlants.Add(9,"firethornSEED");
	    namePlants.Add(10,"giloySEED");
	    namePlants.Add(11,"ninrootSEED");
	    namePlants.Add(12,"aloeSPROUT");
	    namePlants.Add(13,"oranSPROUT");
	    namePlants.Add(14,"potatoSPROUT");
	    namePlants.Add(15,"echinaceaSPROUT");
	    namePlants.Add(16,"lavenderSPROUT");
	    namePlants.Add(17,"chamomileSPROUT");
	    namePlants.Add(18,"krishnaSPROUT");
	    namePlants.Add(19,"firethornSPROUT");
	    namePlants.Add(20,"giloySPROUT");
	    namePlants.Add(21,"ninrootSPROUT");
	    namePlants.Add(22,"aloePLANT");
	    namePlants.Add(23,"oranPLANT");
	    namePlants.Add(24,"potatoPLANT");
	    namePlants.Add(25,"echinaceaPLANT");
	    namePlants.Add(26,"lavenderPLANT");
	    namePlants.Add(27,"chamomilePLANT");
	    namePlants.Add(28,"krishnaPLANT");
	    namePlants.Add(29,"firethornPLANT");
	    namePlants.Add(30,"giloyPLANT");
	    namePlants.Add(31,"ninrootPLANT");
	    namePlants.Add(32,"Needs LIME");
	    namePlants.Add(33,"Needs Fertilizer");
    }
}
