#include <stdio.h>
#include <iostream>
#include <ctime>
#include <map>
#include <unistd.h>

using namespace std;

//Used for printing the plot names
int plot1 = 0;
int plot2 = 0;
int plot3 = 0;
int plot4 = 0;
int plot5 = 0;
int plot6 = 0;

//Constant variables for plants, listed as food, water, light, pH level, and temperature
int arrConstantaloe[6] = {1100, 50, 1000, 6, 70};
int arrConstantoran[6] = {20, 30, 1000, 7, 70};
int arrConstantpotato[6] = {1100, 50, 1000, 6, 70};
int arrConstantechinacea[6] = {900, 60, 1000, 7, 70};
int arrConstantlavender[6] = {700, 50, 800, 6, 70};
int arrConstantchamomile[6] = {1000, 70, 800, 7, 70};
int arrConstantkrishna[6] = {1000, 60, 800, 6, 70};
int arrConstantfirethorn[6] = {800, 100, 1000, 7, 70};
int arrConstantgiloy[6] = {600, 50, 800, 6, 70};
int arrConstantninroot[6] = {1500, 100, 1000, 8, 70};

//Plot details: ID of plant, condition, and current status
int arrPlot1[7] = {0, 0, 0, 0, 0, 0};
int arrPlot2[7] = {0, 0, 0, 0, 0, 0};
int arrPlot3[7] = {0, 0, 0, 0, 0, 0};
int arrPlot4[7] = {0, 0, 0, 0, 0, 0};
int arrPlot5[7] = {0, 0, 0, 0, 0, 0};
int arrPlot6[7] = {0, 0, 0, 0, 0, 0};

//Map for name display for plots
map<int, string> namePlants;

void mappingFunction()
{
	namePlants.insert ( std::pair<int, string>(1,"Empty") );
	namePlants.insert ( std::pair<int, string>(2,"aloeSEED") );
	namePlants.insert ( std::pair<int, string>(3,"oranSEED") );
	namePlants.insert ( std::pair<int, string>(4,"potatoSEED") );
	namePlants.insert ( std::pair<int, string>(5,"echinaceaSEED") );
	namePlants.insert ( std::pair<int, string>(6,"lavenderSEED") );
	namePlants.insert ( std::pair<int, string>(7,"chamomileSEED") );
	namePlants.insert ( std::pair<int, string>(8,"krishnaSEED") );
	namePlants.insert ( std::pair<int, string>(9,"firethornSEED") );
	namePlants.insert ( std::pair<int, string>(10,"giloySEED") );
	namePlants.insert ( std::pair<int, string>(11,"ninrootSEED") );
	namePlants.insert ( std::pair<int, string>(12,"aloeSPROUT") );
	namePlants.insert ( std::pair<int, string>(13,"oranSPROUT") );
	namePlants.insert ( std::pair<int, string>(14,"potatoSPROUT") );
	namePlants.insert ( std::pair<int, string>(15,"echinaceaSPROUT") );
	namePlants.insert ( std::pair<int, string>(16,"lavenderSPROUT") );
	namePlants.insert ( std::pair<int, string>(17,"chamomileSPROUT") );
	namePlants.insert ( std::pair<int, string>(18,"krishnaSPROUT") );
	namePlants.insert ( std::pair<int, string>(19,"firethornSPROUT") );
	namePlants.insert ( std::pair<int, string>(20,"giloySPROUT") );
	namePlants.insert ( std::pair<int, string>(21,"ninrootSPROUT") );
	namePlants.insert ( std::pair<int, string>(22,"aloePLANT") );
	namePlants.insert ( std::pair<int, string>(23,"oranPLANT") );
	namePlants.insert ( std::pair<int, string>(24,"potatoPLANT") );
	namePlants.insert ( std::pair<int, string>(25,"echinaceaPLANT") );
	namePlants.insert ( std::pair<int, string>(26,"lavenderPLANT") );
	namePlants.insert ( std::pair<int, string>(27,"chamomilePLANT") );
	namePlants.insert ( std::pair<int, string>(28,"krishnaPLANT") );
	namePlants.insert ( std::pair<int, string>(29,"firethornPLANT") );
	namePlants.insert ( std::pair<int, string>(30,"giloyPLANT") );
	namePlants.insert ( std::pair<int, string>(31,"ninrootPLANT") );
	namePlants.insert ( std::pair<int, string>(32,"Needs LIME") );
	namePlants.insert ( std::pair<int, string>(33,"Needs Fertilizer") );

}

void printPLOT()
{
	cout << "|||---" << namePlants.at(plot1) << "---|||";
	cout << "\n";
	cout << "|||---" << namePlants.at(plot2) << "---|||";
	cout << "\n";
	cout << "|||---" << namePlants.at(plot3) << "---|||";
	cout << "\n";
	cout << "|||---" << namePlants.at(plot4) << "---|||";
	cout << "\n";
	cout << "|||---" << namePlants.at(plot5) << "---|||";
	cout << "\n";
	cout << "|||---" << namePlants.at(plot6) << "---|||";
	cout << "\n";
}

void plotShift()
{
	for (int m = 0; m < 6; m++)
		 {
		switch(m)
					{
					  case 0:
						  if (plot1 == 22)
						  {
						  plot1 = 1;
						  }
						  else
						  {
							  plot1 = plot1 + 10;
						  }
					    break;
					  case 1:
						  if (plot2 == 22)
							  {
							  plot2 = 1;
							  }
							  else
							  {
								  plot2 = plot2 + 10;
							  }
					    break;
					  case 2:
						  if (plot3 == 22)
							  {
							  plot3 = 1;
							  }
							  else
							  {
								  plot3 = plot3 + 10;
							  }
					  	break;
					  case 3:
						  if (plot4 == 22)
							  {
							  plot4 = 1;
							  }
							  else
							  {
								  plot4 = plot4 + 10;
							  }
					  	break;
					  case 4:
						  if (plot5 == 22)
							  {
							  plot5 = 1;
							  }
							  else
							  {
								  plot5 = plot5 + 10;
							  }
					  	break;
					  case 5:
						  if (plot6 == 22)
							  {
							  plot6 = 1;
							  }
							  else
							  {
								  plot6 = plot6 + 10;
							  }
					  	break;
					}
		 }
}

void ArielMain()
{
	for (int w = 0; w < 10; w++)
	{
		printPLOT();

		plotShift();

		sleep(5);
	}
}

int main()
{

	//Sets up mapping details
	mappingFunction();

	cout << "Ariel, Gardening A.I. Beta \n";
	cout << "By Jared Nelson \n";
	cout << "\n";

	cout << "This current build demonstrates the normal processes of the A.I. to check changing conditions. Please choose your first starter plants.\n";

	for (int i = 0; i < 6; i++)
	 {
		cout << "1. Empty\n";
		cout << "2. Aloe Vera\n";
		cout << "3. Oran Berry\n";
		cout << "4. Potato\n";
		cout << "5. Echinacea\n";
		cout << "6. Lavender\n";
		cout << "7. Chamomile\n";
		cout << "8. Krishna Tulsi\n";
		cout << "9. Firethorn\n";
		cout << "10. Giloy\n";
		cout << "11. Crimson Ninroot\n";

			switch(i)
			{
			  case 0:
				  cin >> plot1;
				  cout << "\n";
			    break;
			  case 1:
				  cin >> plot2;
				  cout << "\n";
			    break;
			  case 2:
				  cin >> plot3;
				  cout << "\n";
			  	break;
			  case 3:
				  cin >> plot4;
				  cout << "\n";
			  	break;
			  case 4:
				  cin >> plot5;
				  cout << "\n";
			  	break;
			  case 5:
				  cin >> plot6;
				  cout << "\n";
			  	break;
			}
	 }

	ArielMain();

}



