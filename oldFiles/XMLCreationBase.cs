using System;
using System.IO;
using System.Xml;

namespace XMLCreationBase
{
    class Program
    {
        static void Main(string[] args)
        {
            //Decalre a new XMLDocument object
            XmlDocument doc = new XmlDocument();

            //create the root element
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //string.Empty makes cleaner code
            XmlElement element1 = doc.CreateElement(string.Empty, "ArielDataFile", string.Empty);
            doc.AppendChild(element1);

            XmlElement element2 = doc.CreateElement(string.Empty, "Constants", string.Empty);
            element1.AppendChild(element2);

            XmlElement element3 = doc.CreateElement(string.Empty, "Aloe", string.Empty);
            XmlText text1 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element3);
            element3.AppendChild(text1);

            XmlElement element4 = doc.CreateElement(string.Empty, "Oran Berry", string.Empty);
            XmlText text2 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element4);
            element4.AppendChild(text2);

            XmlElement element5 = doc.CreateElement(string.Empty, "Potato", string.Empty);
            XmlText text3 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element5);
            element5.AppendChild(text3);

            XmlElement element6 = doc.CreateElement(string.Empty, "Echinacea", string.Empty);
            XmlText text4 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element6);
            element6.AppendChild(text4);

            XmlElement element7 = doc.CreateElement(string.Empty, "Lavender", string.Empty);
            XmlText text5 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element7);
            element7.AppendChild(text5);

            XmlElement element8 = doc.CreateElement(string.Empty, "Chamomile", string.Empty);
            XmlText text6 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element8);
            element8.AppendChild(text6);

            XmlElement element9 = doc.CreateElement(string.Empty, "Krishna", string.Empty);
            XmlText text7 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element9);
            element9.AppendChild(text7);

            XmlElement element10 = doc.CreateElement(string.Empty, "Firethorn", string.Empty);
            XmlText text8 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element10);
            element10.AppendChild(text8);

            XmlElement element11 = doc.CreateElement(string.Empty, "Giloy", string.Empty);
            XmlText text9 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element11);
            element11.AppendChild(text9);

            XmlElement element12 = doc.CreateElement(string.Empty, "Ninroot", string.Empty);
            XmlText text10 = doc.CreateTextNode("Demo Text");
            element2.AppendChild(element12);
            element12.AppendChild(text10);
            

            doc.Save(Directory.GetCurrentDirectory() + "//document.xml");
        }
    }
}